# Описание
Программа позволяет расставлять маркеры на кадра видеофайла. Необходимо открыть Settings и выбрать там требуемое количество маркеров, а также путь к видеофайлу.
После этого надо сохранить настройки и нажать 'Start'. Откроется первый кадр видеофайла, левой кнопкой маркеры можно расставлять маркеры, а правой - удалять 
(удаление происходит по порядку, начиная с последнего выставленного). Если маркер не виден на изображении, его следует указывать сочетанием Ctrl+левая кнопка мыши. Для перехода
к следующему кадру после расставления всех маркеров на данном кадре необходимо нажать клавишу Пробел


# Требования
- Python 3
- Pillow 4.3.0
- opencv-python 3.4.0.12
