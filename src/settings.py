from tkinter import *
from tkinter.filedialog import askdirectory

settings_file = '../data/settings'

global num_pts
num_pts = 68
global filepath
filepath = '../data/'


def read_settings():
    with open(settings_file) as f:
        t = f.readlines()
        if t:
            global num_pts
            num_pts = int(t[0].split()[1])
            global filepath
            filepath = t[1][9:]

read_settings()


class Settings(Frame):
    def __init__(self, parent=None, **options):
        Frame.__init__(self, parent, options)
        self.parent = parent
        self.pack()
        read_settings()

        # row with number of points
        row1 = Frame(self)
        l1 = Label(row1, text='Number of points', width=15)
        l1.pack(side=LEFT)
        self.e1 = Entry(row1)
        self.e1.insert(0, str(num_pts))
        self.e1.config(width=5)
        self.e1.pack(side=RIGHT, expand=YES, anchor=W)
        row1.pack(side=TOP, fill=X)

        # row with file path
        row2 = Frame(self)
        l2 = Label(row2, text='File path', width=15)
        l2.pack(side=LEFT)
        self.e2 = Entry(row2, width=50)
        self.e2.insert(0, filepath)
        self.e2.pack(side=LEFT, expand=YES, fill=X)
        self.browse_button = Button(row2, text='Open file', command=self.browse_file)
        self.browse_button.pack(side=RIGHT, expand=YES, padx=5)
        row2.pack(side=TOP, fill=X)

        # OK and cancel button
        row3 = Frame(self)
        cancel = Button(row3, text='Cancel', command=parent.destroy)
        cancel.pack(side=RIGHT, expand=YES)
        ok = Button(row3, text='OK', command=self.save_settings)
        ok.pack(side=LEFT, expand=YES)
        row3.pack(side=TOP, fill=X)

    def save_settings(self):
        global num_pts
        num_pts = int(self.e1.get())
        global filepath
        filepath = self.e2.get()
        with open(settings_file, 'w') as f:
            text = 'num_pts ' + str(num_pts) +\
                '\nfilepath ' + filepath
            f.write(text)
        self.parent.destroy()

    def browse_file(self):
        path = askdirectory()
        # if new filepath is given, change path
        # in entry field
        if path:
            self.e2.delete(0, len(self.e2.get()))
            self.e2.insert(0, path)


if __name__ == '__main__':
    root = Tk()
    root.title('Settings')
    s = Settings(root)
    s.mainloop()
