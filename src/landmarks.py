from tkinter import *
from PIL.ImageTk import PhotoImage


class LandmarksFrame(Frame):
    def __init__(self, parent=None, image=None, numpts=68, **options):
        Frame.__init__(self, parent, options)
        if not image:
            image = PhotoImage(file='../data/face.jpg')
        self.image = image
        self.landmarks = []
        self.coordinates = []
        self.numpts = numpts
        self.counter = 0
        if image.width() < image.height():
            self.dimension = image.width()
        else:
            self.dimension = image.height()
        self.canvas = Canvas(self, width=image.width(),
                             height=image.height())
        self.id = self.canvas.create_image(round(image.width()/2), round(image.height()/2),
                                 image=self.image)
        self.canvas.pack(side=LEFT)
        self.canvas.bind('<Button-1>', lambda event, invisible=False: self.draw(event, invisible))
        self.canvas.bind('<Control-Button-1>', lambda event, invisible=True: self.draw(event, invisible))
        self.canvas.bind('<Button-3>', self.undraw)
        self.pts_left = Label(self, text=('Points Left:\n%i' % (self.numpts - self.counter)),
                              width=10)
        self.pts_left.pack(anchor=N)
        #self.pack()

    def draw(self, event, invisible):
        x = event.x
        y = event.y
        scale_factor = 0.012
        if self.counter < self.numpts:
            last_landmark = self.canvas.create_oval(x - round(self.dimension * scale_factor / 2),
                                                    y - round(self.dimension * scale_factor / 2),
                                                    x + round(self.dimension * scale_factor / 2),
                                                    y + round(self.dimension * scale_factor / 2),
                                                    fill='white')
            last_point = self.canvas.create_oval(x - round(self.dimension * scale_factor / 8),
                                                    y - round(self.dimension * scale_factor / 8),
                                                    x + round(self.dimension * scale_factor / 8),
                                                    y + round(self.dimension * scale_factor / 8),
                                                    fill='black')
            if invisible:
                self.canvas.itemconfig(last_landmark, fill='yellow')
                self.coordinates.append((self.counter + 1, -1, -1))
            else:
                self.coordinates.append((self.counter + 1, x, y))
            last_label = self.canvas.create_text(x, y + 12, text=str(self.counter + 1), fill='red')
            self.landmarks.append((last_landmark, last_point, last_label))
            self.counter += 1
            self.pts_left['text'] = 'Points Left:\n%i' % (self.numpts - self.counter)

    def undraw(self, event):
        if self.counter > 0:
            landmark, point, label = self.landmarks[self.counter - 1]
            self.canvas.delete(landmark)
            self.canvas.delete(point)
            self.canvas.delete(label)
            self.landmarks.pop(self.counter - 1)
            self.coordinates.pop(self.counter - 1)
            self.counter -= 1
            self.pts_left['text'] = 'Points Left:\n%i' % (self.numpts - self.counter)

    def how_much_landmarks(self):
        return self.counter


if __name__ == '__main__':
    root = Tk()
    l = LandmarksFrame(parent=root)
    l.pack()
    root.mainloop()
