from tkinter import *
from tkinter.messagebox import *
from PIL.ImageTk import PhotoImage
from glob import glob
import settings
from landmarks import LandmarksFrame

start_b_counter = 0
frames_count = 1
frames = []

output_preamble = '''<?xml version='1.0' encoding='ISO-8859-1'?>
<?xml-stylesheet type='text/xsl' href='image_metadata_stylesheet.xsl'?>
<dataset>
<name>Training faces</name>
<images>
'''

output_ending = '''</images>
</dataset>
'''

output_filename = 'my_dlib_training.xml'


def open_settings():
    sw = Toplevel(root)
    sw.title('Settings')
    settings.Settings(parent=sw)
    # make the settings window modal
    sw.focus_set()
    sw.grab_set()
    sw.wait_window()


def quit_b():
    if askyesno('Quit', 'Do you really want to close the program?'):
        qb.quit()


def start_processing():
    img_folder = settings.filepath
    global img_list
    img_list = glob(img_folder + '/*.jpg')
    imgTk = PhotoImage(file=img_list[0])
    l.destroy()
    global landmark_frame
    landmark_frame = LandmarksFrame(parent=root, image=imgTk, numpts=settings.num_pts)
    landmark_frame.pack(side=BOTTOM)
    global images_count_label
    images_count_label = Label(f, text='Image {0} of {1}'.format(frames_count, len(img_list)), bg='white')
    images_count_label.config(bd=3, relief=FLAT)
    images_count_label.pack(side=RIGHT, padx=3, anchor=NE)
    with open('../data/saved_files/{0}'.format(output_filename), 'w') as fout:
        fout.write(output_preamble)


def next_frame(event):
    try:
        global landmark_frame
        if landmark_frame.how_much_landmarks() != settings.num_pts:
            showwarning('Error', '{0} of {1} landmark(s) are done\nAdd {2} more landmark(s) to continue'.format(
                landmark_frame.how_much_landmarks(), settings.num_pts, settings.num_pts - landmark_frame.how_much_landmarks()
            ))
        else:
            global frames_count
            image = img_list[frames_count - 1].split('\\')[1]
            imageTk = PhotoImage(file=img_list[frames_count])
            import os
            if not os.path.exists('../data/saved_files'):
                os.mkdir('../data/saved_files')
            with open('../data/saved_files/{0}'.format(output_filename), 'a+') as fout:
                fout.write("  <image file='{0}'>\n    <box top='1' left='1' width='{1}' height='{2}'>\n".format(image, imageTk.width() - 3, imageTk.height() - 3))
                for point in landmark_frame.coordinates:
                    no, x, y = point
                    if no - 1 < 10:
                        fout.write("      <part name='0{0}' x='{1}' y='{2}'/>\n".format(no - 1, x, y))
                    else:
                        fout.write("      <part name='{0}' x='{1}' y='{2}'/>\n".format(no - 1, x, y))
                fout.write('    </box>\n  </image>\n')
            if frames_count < len(img_list):
                landmark_frame.destroy()
                landmark_frame = LandmarksFrame(parent=root, image=imageTk, numpts=settings.num_pts)
                landmark_frame.pack(side=BOTTOM)
                frames_count += 1
                images_count_label['text'] = 'Image {0} of {1}'.format(frames_count, len(img_list))
            else:
                with open('../data/saved_files/{0}'.format(output_filename), 'a+') as fout:
                    fout.write(output_ending)
                showinfo('The end', 'All frames of this video are processed')
    except NameError as e:
        showinfo('Warning', 'Please press start button and put all the landmarks\non the image')


root = Tk()
root.title('Image Annotation')
root.bind('<space>', next_frame)

f = Frame(root, bg='white')

start_button = Button(f, text='Start', bg='white', command=start_processing)
start_button.config(bd=3, relief=FLAT)
start_button.pack(side=LEFT, padx=3, anchor=NW)

sb = Button(f, text='Settings', bg='white', command=open_settings)  # settings button
sb.config(bd=3, relief=FLAT)
sb.pack(side=LEFT, padx=3, anchor=NW)

qb = Button(f, text='Quit', bg='white', command=quit_b)             # quit button
qb.config(bd=3, relief=FLAT)
qb.pack(side=LEFT, padx=3, anchor=NW)

f.config(bd=1, relief=SOLID)
f.pack(side=TOP, anchor=NW, fill=X)
l = Label(root, width=50, height=30)
l.pack(side=BOTTOM)
root.mainloop()
