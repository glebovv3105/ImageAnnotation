import cv2
from tkinter import *
from PIL.ImageTk import PhotoImage
from PIL import Image


frames = []
frames_count = 1


def callback(event):
    global frames_count
    if frames_count < len(frames):
        l['image'] = frames[frames_count]
        frames_count += 1


root = Tk()
cap = cv2.VideoCapture('../data/test9.avi')
ret, frame = cap.read()
while ret:
    ret, first_frame = cap.read()
    if not ret:
        break
    img = Image.fromarray(first_frame)
    imgTk = PhotoImage(img)
    frames.append(imgTk)

l = Label(root, image=frames[0])
l.bind('<Button-1>', callback)
l.pack()
root.mainloop()
